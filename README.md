# 051-pipeline-ejemplo

## Docker commands local
```
- docker build -t hola-node .
- docker run hola-node
- docker run -p 8080:8080 hola-node
```
## Docker commands registry
```
- docker login registry.gitlab.com
- docker pull registry.gitlab.com/pipelines13571929/051-pipeline-ejemplo:main-eafb497d
docker run -p 8080:8080 registry.gitlab.com/pipelines13571929/051-pipeline-ejemplo:main-eafb497d
```